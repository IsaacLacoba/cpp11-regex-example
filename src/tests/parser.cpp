// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "bandit/bandit.h"

#include "parser.h"

using namespace bandit;

go_bandit([] {
    describe("a parser", [] {
        Parser parser;

        it("can read file with a determinate structure", [&]() {

            /*
                      A match_results provides access to its match string, its sub_matches ,
                      and the characters before and
                      after the match

                      (page 1060 from "The C++ Programming Language.
                      Fourth Edition- Bjarne Stroustrup- Addision-Wesley)

                           -----------------------------------
                          |               m[0]                |
                -----------------------------------------------------------
               | m.prefix | m[1] | m[2] | ... |  m[m.size()]  | m.suffix() |
               ------------------------------------------------------------
            */
            parser.open("data/data.txt");
            //Format: lineID: explanationLine (Fail|Pass)
            std::string pattern = R"((\d*):\s(.*)\s(Pass|Fail))";

            parser.inject(pattern);

            Parser::Results results = parser.get_results();

            for(auto result: results)
              Assert::That(result[3], Equals("Pass"));

          });

        it("another example that fails on purpose", [&]() {
            Parser circuit_parser;
            circuit_parser.inject(R"(section: \{\sposition: \{(-?\b\d*.\d*), (-?\b\d*.\d*), (-?\b\d*.\d*)\}\s*rotation: \{(-?\d{1,3})\}\s*type: \{(\w*)\} \})");
            circuit_parser.open("data/circuit.data");
            Parser::Results results = circuit_parser.get_results();
            std::cout << results.size() << std::endl;
            for(auto result: results) {
              std::cout << "matches: ";
              for(auto group: result)
                std::cout
                  << "\n\tgroup: " << group;
              std::cout << std::endl;
            }

            Assert::That(1, Equals(2));
          });
      });
  });

int main(int argc, char *argv[]) {
  return bandit::run(argc, argv);
}
