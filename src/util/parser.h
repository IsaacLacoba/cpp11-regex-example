// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#ifndef _PARSER_H_
#define _PARSER_H_
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>

class Parser {
  std::ifstream file_;
  std::regex pattern_;
 public:
  typedef std::vector<std::string> Match;
  typedef std::vector<Match> Results;

  Parser();
  virtual ~Parser();

  void open(std::string file);
  void inject(std::string pattern);
  Results get_results();
};

#endif /* _PARSER_H_ */
