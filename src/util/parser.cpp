// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "parser.h"

Parser::Parser() {

}

Parser::~Parser() {

}

void
Parser::open(std::string file) {
  file_.open(file);
  if (!file_.is_open()) {
    std::cerr << "no file: \n"
              << file
              << std::endl;
    exit(-1);
  }
}

void
Parser::inject(std::string pattern) {
  try {
    pattern_ = std::regex{pattern};
  } catch(std::regex_error& exception) {
    std::cout << "Exception:" << exception.code()
              << "\nreason: " << exception.what()
              << std::endl;
      exit(-1);
  }
}

Parser::Results
Parser::get_results() {
  Parser::Results results;

  std::smatch matches;
  for (std::string line; getline(file_, line);) {
    if(std::regex_search(line, matches, pattern_)) {
      Parser::Match string_matches;
      string_matches.insert(string_matches.begin(), matches.begin(), matches.end());
      results.push_back(string_matches);
    }
  }
  return results;
}
